/**
 * @file Implements the functionality of the calculator.
 * @author Kieran Blazier
 */

document.addEventListener( "DOMContentLoaded", function () {
    
    var result = document.querySelector(".result");
    var controls = document.querySelector(".controls");
    
    var tokens = [];

    var operators = { "+": { prec: 1, func: (x, y) => x + y },
		      "-": { prec: 1, func: (x, y) => x - y },
		      "*": { prec: 2, func: (x, y) => x * y },
		      "/": { prec: 2, func: (x, y) => x / y }
		    }

    var actions = { "input" : processInput,
		    "submit" : submit,
		    "clear" : clear
		  }

    var input = "0";
    var prevResult = "";

    /**
    * Displays the given string in the result pane
    */
    function display(str) {
	result.innerHTML = str;
    }

    /** 
    * Returns whether the given token is an operator
    * @param {string} tok - The token to test
    */
    function isOperator(tok) {
	return ["*","/","+","-"].includes(tok);
    }


    /**
    * Converts a sequence of tokens in infix notation to postfix respecting operator precedence
    * @param {string[]} tokens - The seqeunce of tokens
    */
    function shuntingYard(tokens) {
	var output = [];
	var opstack = [];

	tokens.forEach(tok => {
	    if(!isOperator(tok)) {
		output.push(tok);
            } else {
		while(opstack.length > 0 && operators[opstack[0]].prec > operators[tok].prec) {
		    output.push(opstack.pop());
		}

		opstack.push(tok);
	    }
	});

	while(opstack.length > 0) {
	    output.push(opstack.pop());
	}

	return output;
    }

    /**
    * Evaluates an expression in postfix notation.
    * @param {string[]} tokens - The sequence of tokens representing the expresion
    */
    function evaluateRPN(tokens) {
	var stack = [];

	tokens.forEach(tok => {
	    if(!isOperator(tok)) {
		stack.push(tok);
            } else {
		var y = parseFloat(stack.pop());
		var x = parseFloat(stack.pop());
		stack.push(operators[tok].func(x, y));
	    }
	});

	return stack.pop();
    }

    /**
    * Evaluates an expression in infix notation, respecting operator precedence.
    * @param {string[]} tokens - The sequence of tokens representing the expresion
    */
    function evaluate(tokens) {
	return evaluateRPN(shuntingYard(tokens));
    }

    /**
    * Clears the calculator state
    */
    function clear() {
	tokens = [];
	input = "0";
	prevResult = "";
	display(input);
    }

    /**
    * Processes an input button.
    * @param {Object} btn - The DOM node of the button that was pressed.
    */
    function processInput(btn) {
	var op = btn.dataset.op;
	//If the input is an operator
	if(op != null) {
	    //If there is an unprocessed input
	    if(input.length > 0) {
		//Add the input to the list of tokens
		tokens.push(input);
	    } else { //Otherwise add the result of the previous calculation to the list of tokens
		tokens.push(prevResult);
	    }

	    //Display intermediate results
	    var intermediateResult = evaluate(tokens);
	    display(intermediateResult);

	    //Add the operator to the list of tokens, and clear the input buffer
	    tokens.push(op);
	    input = "";    
	} else if(input != "0") { //If the input buffer is not just "0", then concatenate the input to the input buffer
	    input = input + btn.innerHTML;
	    display(input);
	} else { //Otherwise the input is just "0", so to avoid leading zeros, simply replace the input buffer
	    input = btn.innerHTML;
	    display(input);
	}
    }


    /**
    * Evaluates and displays the current computation.
    */
    function submit() {
	//Add the current input to the list of tokens
	tokens.push(input);

	//Evaluate the expression
	prevResult = evaluate(tokens);
	
	//If the result is a floating point Infinity, a division by zero occcured, so display "Undefined"
	if (!Number.isFinite(prevResult)) {
	    display("Undefined");
	} else if(prevResult.toString().split('.')[0].length > 10) { //If the integer part of the number exceeds 10 digits, display "Error"
	    display("Error");
	} else { //Otherwise, display the result
	    display(prevResult);
	}
	 
	//Reset the current expression and the input buffer
	tokens = [];
	input = "";
    }

    controls.addEventListener( "click", e => {
	var btn = e.target;

	btn.classList.add("flash");
	setTimeout( () => {
	    btn.classList.remove("flash");
        }, 500);

	actions[btn.dataset.action](btn);
    }, false);
});
